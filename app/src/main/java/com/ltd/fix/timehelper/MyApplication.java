package com.ltd.fix.timehelper;

import android.app.Application;

/**
 * Created by fix on 31.01.16.
 */
public class MyApplication extends Application {
    private static boolean activityVisible;

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }
}
